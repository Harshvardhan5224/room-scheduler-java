
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class WaitlistQueries {
    private static Connection connection;
    private static PreparedStatement getWaitlist;
    private static ResultSet resultSet;
    private static PreparedStatement addWaitlist;
    private static PreparedStatement searchWaitlist;
    private static PreparedStatement getWaitlistByName;
    private static PreparedStatement deleteWaitlist;
    public static List<WaitlistEntry> showWaitlist(){
        List<WaitlistEntry> list= new ArrayList<WaitlistEntry>();
        connection = DBConnection.getConnection();
        try{
            getWaitlist=connection.prepareStatement("SELECT * FROM Waitlist ORDER BY date,TimeStamp");
            resultSet=getWaitlist.executeQuery();
            while(resultSet.next()){
                list.add(new WaitlistEntry(resultSet.getString(1),resultSet.getDate(2),resultSet.getInt(3),resultSet.getTimestamp(4)));
            }
            
        }
        catch(SQLException sqlException)
        {
            sqlException.printStackTrace();
        }
        return list;
    }
    
    public static void addWaitlist(String name, Date date, int seat, java.sql.Timestamp timestamp){
        connection=DBConnection.getConnection();
        try{
            addWaitlist=connection.prepareStatement("insert into Waitlist (Faculty, Date, Seats, TimeStamp) values(?,?,?,?)");
            addWaitlist.setString(1,name);
            addWaitlist.setDate(2,date);
            addWaitlist.setInt(3, seat);
            addWaitlist.setTimestamp(4,timestamp);
            addWaitlist.executeUpdate();
        }
        catch(SQLException sqlException)
        {
            sqlException.printStackTrace();
        }
        
    }
    
    public static int searchWaitlist(String name,Date date){
        connection = DBConnection.getConnection();
        List<WaitlistEntry> list=new ArrayList<WaitlistEntry>();
        try
        {
            searchWaitlist= connection.prepareStatement("SELECT * FROM Waitlist WHERE faculty=? and Date=?");
            searchWaitlist.setString(1,name);
            searchWaitlist.setDate(2, date);
            resultSet=searchWaitlist.executeQuery();
           
            while(resultSet.next()){
                list.add(new WaitlistEntry(resultSet.getString(1),resultSet.getDate(2),resultSet.getInt(3),resultSet.getTimestamp(4)));
            }
            
            if(list.isEmpty()){
                return 1;
            }
            else{
                return 0;
            }
            
        }
        catch(SQLException sqlException)
        {
            sqlException.printStackTrace();
            return -1;
        }
        
    }
    public static List<WaitlistEntry> showWaitlistByName(String name){
        List<WaitlistEntry> list= new ArrayList<WaitlistEntry>();
        connection = DBConnection.getConnection();
        try{
            getWaitlistByName=connection.prepareStatement("SELECT * FROM Waitlist WHERE faculty=?");
            getWaitlistByName.setString(1,name);
            resultSet=getWaitlistByName.executeQuery();
            while(resultSet.next()){
                list.add(new WaitlistEntry(resultSet.getString(1),resultSet.getDate(2),resultSet.getInt(3),resultSet.getTimestamp(4)));
            }
            
        }
        catch(SQLException sqlException)
        {
            sqlException.printStackTrace();
        }
        return list;
    }
    
    public static int deleteWaitlist(String name, Date date){
        connection = DBConnection.getConnection();
            try{
                deleteWaitlist=connection.prepareStatement("DELETE FROM Waitlist WHERE faculty=? and date=?");
                deleteWaitlist.setString(1,name);
                deleteWaitlist.setDate(2,date);
                deleteWaitlist.executeUpdate();
                return 1;
            
            }
            catch(SQLException sqlException)
            {
                sqlException.printStackTrace();
                return 0;
            }
    }
}
