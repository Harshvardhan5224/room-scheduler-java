
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class RoomEntry {
    private String roomName;
    private int seats;
    private static Connection connection;
    
    private static PreparedStatement addRoom;
    private static PreparedStatement getFacultyList;
    private static ResultSet resultSet;
    private static PreparedStatement getRoom;
    private static PreparedStatement dropRoom;   
    private static PreparedStatement searchRoom;
    private static PreparedStatement dropReservation;
    public RoomEntry(String name, int seats){
        roomName=name;
        this.seats=seats;
    }
    public RoomEntry(){
        
    }
    
    //setter methods
    public void setRoomName(String name){
        roomName=name;
    }
    public void setSeats(int seats){
        this.seats=seats;
    }
    
    //getter methods
    public String getRoomName(){
        return roomName;
    }
    public int getSeats(){
        return seats;
    }
    public static int addRoom(String name, int num){
        connection = DBConnection.getConnection();
        try
        {
            addRoom = connection.prepareStatement("insert into rooms (name, seats) values (?,?)");
            addRoom.setString(1, name);
            addRoom.setInt(2, num);
            addRoom.executeUpdate();
            return 1;
        }
        catch(SQLException sqlException)
        {
            sqlException.printStackTrace();
            return -1;
        }
    }
    
    public static List<String> showRoom(){
        connection = DBConnection.getConnection();
        ArrayList<String> list = new ArrayList<String>();
        try
        {
            getRoom = connection.prepareStatement("SELECT name FROM rooms");
            resultSet = getRoom.executeQuery();
            
            while(resultSet.next())
            {
                list.add(resultSet.getString(1));
            }
        }
        catch(SQLException sqlException)
        {
            sqlException.printStackTrace();
        }
        return list;
    }
    
    public static void dropRoom(String room){
        connection = DBConnection.getConnection();
        List<ReservationEntry> list=new ArrayList<ReservationEntry>();
        try
        {
            dropRoom = connection.prepareStatement("DELETE FROM rooms WHERE name=?");
            dropRoom.setString(1, room);
            dropRoom.executeUpdate();
            System.out.println("hey1");
        }
        catch(SQLException sqlException)
        {
            sqlException.printStackTrace();
        }
        try
        {
            searchRoom = connection.prepareStatement("SELECT * FROM reservations where room=?");
            searchRoom.setString(1, room);
            resultSet=searchRoom.executeQuery();
            System.out.println("hey2");
          
            while(resultSet.next()){
                list.add(new ReservationEntry(resultSet.getString(1),resultSet.getString(2),resultSet.getDate(3),resultSet.getInt(4),resultSet.getTimestamp(5)));
            }
            if (!list.isEmpty()){
                try{
                    dropReservation=connection.prepareStatement("DELETE FROM reservations where room=?");
                    dropReservation.setString(1,room);
                    dropReservation.executeUpdate();
                    for(int i=0;i<list.size();i++){
                        String ans=ReservationQueries.addReservation(list.get(i).getFacultyName(),list.get(i).getDate(),list.get(i).getSeats());
                    }
                }
                catch(SQLException sqlException)
                {
                    sqlException.printStackTrace();
                }
            }
            
        }
        catch(SQLException sqlException)
        {
            sqlException.printStackTrace();
        }
        
    }
}
