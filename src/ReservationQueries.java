
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class ReservationQueries {
    private static Connection connection;
    private static ArrayList<String> faculty = new ArrayList<String>();
    private static PreparedStatement findOptimalRoom;
    private static PreparedStatement search;
    private static PreparedStatement addReservation;
    private static ResultSet resultSet;
    private static RoomEntry potentialRoom;
    private static java.sql.Timestamp timeStamp;
    private static PreparedStatement addWaitlist;
    private static PreparedStatement getReservation;
    private static PreparedStatement searchReservation;
    private static PreparedStatement getWaitlist;
    private static PreparedStatement deleteWaitlist;
    private static PreparedStatement deleteReservation;
    private static PreparedStatement getReservationByName;
    
    public static String addReservation(String name,Date findDate, int findSeat)
    {
        
        connection = DBConnection.getConnection();
        try
        {
           
            timeStamp=new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
            findOptimalRoom = connection.prepareStatement("SELECT * FROM Rooms WHERE Seats>=? ORDER BY Seats");
            
            findOptimalRoom.setInt(1,findSeat);
            resultSet=findOptimalRoom.executeQuery();
            
            
            List<RoomEntry> roomList=new ArrayList<RoomEntry>();
            while(resultSet.next()){
                roomList.add(new RoomEntry(resultSet.getString(1),resultSet.getInt(2)));
            }
           // if (roomList.isEmpty()){
           //     return "nothing";
           // }
            for(int i=0;i<roomList.size();i++)
            {
                
                potentialRoom=roomList.get(i);
                search=connection.prepareStatement("SELECT * FROM Reservations WHERE Room=? and Date=? ");
        
                search.setString(1,potentialRoom.getRoomName());
                search.setDate(2,findDate);
                resultSet=search.executeQuery();
                List<ReservationEntry> list=new ArrayList<ReservationEntry>();
                while(resultSet.next()){
                    list.add(new ReservationEntry(resultSet.getString(1),resultSet.getString(2),resultSet.getDate(3),resultSet.getInt(4),resultSet.getTimestamp(5)));
                
                }
                if(list.isEmpty()){
                    int count=searchReservation(name,findDate);
                    if (count==1)
                    {
                        addReservation=connection.prepareStatement("insert into Reservations (Faculty, Room, Date, Seats,TimeStamp) Values(?,?,?,?,?)");
                        addReservation.setString(1,name);
                        addReservation.setString(2,potentialRoom.getRoomName());
                        addReservation.setDate(3,findDate);
                        addReservation.setInt(4, potentialRoom.getSeats());
                        addReservation.setTimestamp(5, timeStamp);
                        addReservation.executeUpdate();
                        return potentialRoom.getRoomName();
                    }
                    else if(count==0){
                        return "alreadyReserved";
                    }
                    else{
                        return "error";
                    }
                }
            }
            
            int count=WaitlistQueries.searchWaitlist(name,findDate);
            if (count==1)
            {
                WaitlistQueries.addWaitlist(name, findDate, findSeat, timeStamp);
                return "waitlist";
            }
            else if(count==0){
                return "alreadyWaitlisted";
            }
            else{
                return "error";
            }
                   
            
            
            
        }
        
        catch(SQLException sqlException)
        {
           
            sqlException.printStackTrace();
            return "error";
        }
        
    }
    
    public static List<ReservationEntry> showReservationBydates(Date date){
        connection = DBConnection.getConnection();
        List<ReservationEntry> list=new ArrayList<ReservationEntry>();
        try
        {
            getReservation= connection.prepareStatement("SELECT * FROM Reservations WHERE Date=?");
            getReservation.setDate(1, date);
            resultSet=getReservation.executeQuery();
            
            while(resultSet.next()){
                list.add(new ReservationEntry(resultSet.getString(1),resultSet.getString(2),resultSet.getDate(3),resultSet.getInt(4),resultSet.getTimestamp(5)));
            }
            
        }
        catch(SQLException sqlException)
        {
            sqlException.printStackTrace();
        }
        
        return list;
    }
    
    public static int searchReservation(String name, Date date){
        connection = DBConnection.getConnection();
        List<ReservationEntry> list=new ArrayList<ReservationEntry>();
        try
        {
            searchReservation= connection.prepareStatement("SELECT * FROM Reservations WHERE faculty=? and Date=?");
            searchReservation.setString(1,name);
            searchReservation.setDate(2, date);
            resultSet=searchReservation.executeQuery();
           
            while(resultSet.next()){
                list.add(new ReservationEntry(resultSet.getString(1),resultSet.getString(2),resultSet.getDate(3),resultSet.getInt(4),resultSet.getTimestamp(5)));
            }
            
            if(list.isEmpty()){
                return 1;
            }
            else{
                return 0;
            }
            
        }
        catch(SQLException sqlException)
        {
            sqlException.printStackTrace();
            return -1;
        }
        
    }
    public static void addReservationFromWaitList(){
        
        List<WaitlistEntry> list= new ArrayList<WaitlistEntry>();
        connection = DBConnection.getConnection();
        try{
            getWaitlist=connection.prepareStatement("SELECT * FROM Waitlist ORDER BY TimeStamp");
            resultSet=getWaitlist.executeQuery();
            while(resultSet.next()){
                list.add(new WaitlistEntry(resultSet.getString(1),resultSet.getDate(2),resultSet.getInt(3),resultSet.getTimestamp(4)));
            }
            
        }
        catch(SQLException sqlException)
        {
            sqlException.printStackTrace();
        }
        int temp=-1;
        for(int i=0;i<list.size();i++){
            String ans=addReservation(list.get(i).getName(),list.get(i).getDate(),list.get(i).getSeats());
            if(ans!="waitlist" || ans!="alreadyWaitlisted" || ans!="alreadyReserved"||  ans!="error" ){
                temp=i;
                break;
            }
        }
        
        if (temp!=-1){
            try{
            deleteWaitlist=connection.prepareStatement("DELETE FROM waitlist WHERE faculty=? and date=? and seats=? and timestamp=?");
            deleteWaitlist.setString(1,list.get(temp).getName());
            deleteWaitlist.setDate(2,list.get(temp).getDate());
            deleteWaitlist.setInt(3,list.get(temp).getSeats());
            deleteWaitlist.setTimestamp(4, list.get(temp).getTimestamp());
            deleteWaitlist.executeUpdate();
            
        }
        catch(SQLException sqlException)
        {
            sqlException.printStackTrace();
        }
        }
    }
    public static int cancelReservation(String name, Date date){
        int ans=searchReservation(name, date);
        if (ans==0){
            connection = DBConnection.getConnection();
            try{
                deleteReservation=connection.prepareStatement("DELETE FROM reservations WHERE faculty=? and date=?");
                deleteReservation.setString(1,name);
                deleteReservation.setDate(2,date);
                deleteReservation.executeUpdate();
            
            }
            catch(SQLException sqlException)
            {
                sqlException.printStackTrace();
            }
            return 1;
        }
        else{
            return 0;
        }
        
    }
    public static List<ReservationEntry> showReservationByName(String name){
        connection = DBConnection.getConnection();
        List<ReservationEntry> list=new ArrayList<ReservationEntry>();
        try
        {
            getReservationByName= connection.prepareStatement("SELECT * FROM Reservations WHERE faculty=?");
            getReservationByName.setString(1,name);
            resultSet=getReservationByName.executeQuery();
            
            while(resultSet.next()){
                list.add(new ReservationEntry(resultSet.getString(1),resultSet.getString(2),resultSet.getDate(3),resultSet.getInt(4),resultSet.getTimestamp(5)));
            }
            
        }
        catch(SQLException sqlException)
        {
            sqlException.printStackTrace();
        }
        
        return list;
    }
       
}
