
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class ReservationEntry {
    private String facultyName;
    private String room;
    private Date date;
    private int seats;
    private java.sql.Timestamp timestamp;
   
    
    
    public ReservationEntry(String name,String room, Date date, int seats,java.sql.Timestamp timestamp){
        facultyName=name;
        this.room=room;
        this.date=date;
        this.seats=seats;
        this.timestamp=timestamp;
    }
    //setter methods
    public void setFacultyName(String name){
        facultyName=name;
    }
    
    public void setRoom(String room){
        this.room=room;
    }
    
    public void setDate(Date date){
        this.date=date;
    }
        
    public void setSeats(int seats){
        this.seats=seats;
    }
    
    //getter methods
    public String getFacultyName(){
        return facultyName;
    }
    
    public String getRoom(){
        return room;
    }
    
    public Date getDate(){
        return date;
    }
    
    public int getSeats(){
        return seats;
    }
    
    
}


