
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Date;
import java.text.SimpleDateFormat;


public class Dates {
    private static Connection connection;
    private static ArrayList<String> faculty = new ArrayList<String>();
    private static PreparedStatement addDate;
    private static PreparedStatement getDateList;
    private static ResultSet resultSet;
    
    public static void addDate(Date date)
    {
        connection = DBConnection.getConnection();
        try
        {
            addDate = connection.prepareStatement("insert into Dates (Date) values (?)");
            addDate.setDate(1, date);
            addDate.executeUpdate();
        }
        catch(SQLException sqlException)
        {
            sqlException.printStackTrace();
        }
        
    }
    
    public static ArrayList<Date> getDatelist(){
        connection = DBConnection.getConnection();
        ArrayList<Date> date = new ArrayList<Date>();
        try
        {
            getDateList = connection.prepareStatement("select Date from Dates order by Date");
            resultSet = getDateList.executeQuery();
            
            while(resultSet.next())
            {
                date.add(resultSet.getDate(1));
            }
        }
        catch(SQLException sqlException)
        {
            sqlException.printStackTrace();
        }
        return date;
        
    }
    public static String convertDateToString(Date indate)
    {
        String dateString = null;
        SimpleDateFormat sdfr = new SimpleDateFormat("yyyy-MM-dd");
   
        try{
            dateString = sdfr.format( indate );
        }catch (Exception ex ){
            System.out.println(ex);
        }
        return dateString;
    }
    
    public static Date convertStringToDate(String date){
        SimpleDateFormat formater=new SimpleDateFormat("yyyy-MM-dd");
        String spinnerValue =formater.format(date);
        Date day=Date.valueOf(date);
        return day;
        
    }
    
    
        
}
