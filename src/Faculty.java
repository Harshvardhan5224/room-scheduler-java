
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class Faculty
{
    private static Connection connection;
    
    private static PreparedStatement addFaculty;
    private static PreparedStatement getFacultyList;
    private static ResultSet resultSet;
    private String name;
    
    public Faculty(String name){
        this.name=name;
    }
    public String getName(){
        return name;
    }
    
    
    public static int addFaculty(String name)
    {
        int count=0;
        List<String> list=getFacultyList();
        for(int i=0;i<list.size();i++){
            if(list.get(i).equals(name)){
                count=1;
            }
        }
        if (count==0)
        {
        connection = DBConnection.getConnection();
        try
        {
            addFaculty = connection.prepareStatement("insert into faculty (name) values (?)");
            addFaculty.setString(1, name);
            addFaculty.executeUpdate();
            return 1;
        }
        catch(SQLException sqlException)
        {
            sqlException.printStackTrace();
            return -1;
        }
        
        }
        return 0;
        
        
    }
    
    public static List<String> getFacultyList()
    {
        connection = DBConnection.getConnection();
        ArrayList<Faculty> faculty = new ArrayList<Faculty>();
        List<String> list=new ArrayList<String>();
        try
        {
            getFacultyList = connection.prepareStatement("select name from faculty order by name");
            resultSet = getFacultyList.executeQuery();
            faculty.clear();
            while(resultSet.next())
            {
                faculty.add(new Faculty(resultSet.getString(1)));
                
            }
        }
        catch(SQLException sqlException)
        {
            sqlException.printStackTrace();
        }
        for(int i=0;i<faculty.size();i++){
            list.add(faculty.get(i).getName());
        }
        return list;
        
    }
    
    
}
