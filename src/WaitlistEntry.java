
import java.sql.Date;



public class WaitlistEntry {
    private String name;
    private Date date;
    private int seats;
    private java.sql.Timestamp timestamp;
    
    
    public WaitlistEntry(String name, Date date, int seats,java.sql.Timestamp timestamp ){
        this.name=name;
        this.date=date;
        this.seats=seats;
        this.timestamp=timestamp;
    }
    
    //getter methods
    public String getName(){
        return name;
    }
    public Date getDate(){
        return date;
    }
    public int getSeats(){
        return seats;
        
    }
    public java.sql.Timestamp getTimestamp(){
        return timestamp;
    }
    
    
    
    
}
